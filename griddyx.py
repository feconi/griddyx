#!/usr/bin/env python3

from PIL import Image

def griddyx(img, grid_count=(2,2), pattern="out"):
    grid_size = ( img.size[0] // grid_count[0] , img.size[1] // grid_count[1] )
    img_grid = Image.new(img.mode, grid_size)

    for x in range( grid_count[0] ):
        for y in range( grid_count[1] ):
            print(f"Making grid {x} {y}")
            crop_box = ( x    * grid_size[0],
                         y    * grid_size[1],
                        (x+1) * grid_size[0] -1,
                        (y+1) * grid_size[1] -1
                       )
            grid = img.crop(crop_box)
            img_grid.paste(grid)

            path = f"{pattern}_{x}_{y}.png"
            try:
                img_grid.save(path)
            except OSError:
                print("Error: Image can not be saved. Aborting.")
                return

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('image')
    parser.add_argument('grids_x')
    parser.add_argument('grids_y')

    args = parser.parse_args()

    img = Image.open( args.image )
    pattern = "".join(args.image.split('.')[:-1])
    x = int( args.grids_x )
    y = int( args.grids_y )

    griddyx(img, (x, y), pattern)
